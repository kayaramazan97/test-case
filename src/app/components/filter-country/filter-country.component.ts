import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-filter-country',
  templateUrl: './filter-country.component.html',
  styleUrls: ['./filter-country.component.css']
})
export class FilterCountryComponent implements OnInit {
  public lineChartType:any = 'line';
  public lineChartData:any[]= [
    { data: [], label: '' },
    { data: [], label: '' }, 
  ];
  public lineChartLabels:any[]= [];
  public lineChartOptions:any = {
    responsive: true,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
        },
        {
          id: 'y-axis-1',
          position: 'right',
          gridLines: {
            color: 'rgba(255,0,0,0.3)',
          },
          ticks: {
            fontColor: 'red',
          }
        }
      ]
    },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'vertical',
          scaleID: 'x-axis-0',
          value: 'March',
          borderColor: 'orange',
          borderWidth: 2,
          label: {
            enabled: true,
            fontColor: 'orange',
            content: 'LineAnno'
          }
        },
      ],
    },
  }; 
  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  }; 

  public barChartLabels: any = [];
  public barChartType: any = 'bar';
  public barChartLegend = true;
  public barChartData: any = [
    { data: [], label: '' },
    { data: [], label: '' }
  ];

  countries: string[] = ["belarus", "brazil", "canada", "european-union", "eurozone", "france", "germany", "greece", "india", "japan", "kazakhstan", "mexico", "russia", "spain", "turkey", "ukraine", "united-kingdom", "united-states"]
  constructor(private api: ApiService) { }
  countryResults!: any[]
  resultData!: any[]
  ngOnInit(): void {
  }
  selectCountry(FirstCountry: string, SecondCountry: string, dateStart: string, dateEnd: string): any {
    if (dateStart && dateEnd) {
      this.api.getStatistic(FirstCountry)
        .subscribe((result: any[]) => {
          this.countryResults = result.map(item => { item.MonthFormatted = new Date(item.MonthFormatted); return item })

          this.getCountryByDate(dateStart, dateEnd, FirstCountry,0)
        }).add(() => {
          this.api.getStatistic(SecondCountry)
            .subscribe((result: any[]) => {
              this.countryResults = result.map(item => { item.MonthFormatted = new Date(item.MonthFormatted); return item })

              this.getCountryByDate(dateStart, dateEnd, SecondCountry,1)
            })
        })
    }
    else
      alert('Tarih Giriniz')
  }
  getCountryByDate(dateStart: string, dateEnd: string, country: string,index:number) {
    let sDate = new Date(dateStart)
    let eDate = new Date(dateEnd)
    this.resultData = this.countryResults.filter(item => item.MonthFormatted > sDate && item.MonthFormatted < eDate)
    console.log(this.resultData)
    // Set Bar chart
    this.barChartData[index].data = this.resultData.map(item => item.InflationRate)
    this.barChartData[index].label = country.toUpperCase()
    this.barChartLabels = this.resultData.map(item => item.MonthFormatted.getDate() + "-" + (item.MonthFormatted.getMonth() + 1) + "-" + item.MonthFormatted.getFullYear())

    this.lineChartData[index].data = this.resultData.map(item => item.InflationRate)
    this.lineChartData[index].label = country.toUpperCase()
    this.lineChartLabels = this.resultData.map(item => item.MonthFormatted.getDate() + "-" + (item.MonthFormatted.getMonth() + 1) + "-" + item.MonthFormatted.getFullYear())
 
  }
}
