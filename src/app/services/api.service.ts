import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Country } from '../country';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http:HttpClient) { }
  baseUrl = 'https://www.statbureau.org/get-data-json'
  public getStatistic(country:string):Observable<any>
  {
    return this.http.get(this.baseUrl,{params:{country}})
  }
}
