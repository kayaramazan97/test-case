# TestCase

This project will bring the inflation rate of 2 countries according to the dates.

## Quick Start
`npm install` <br>

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

You can look screenshot from below;

<img src="https://gitlab.com/kayaramazan97/test-case/-/raw/master/Screen%20Shot%202020-12-29%20at%2021.37.04.png" >
<img src="https://gitlab.com/kayaramazan97/test-case/-/raw/master/Screen%20Shot%202020-12-29%20at%2021.37.10.png" >
